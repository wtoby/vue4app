import {
  Toast
} from 'vant'

class Common {
  static loadingRequestCount = 0;
  static _loading = null;

  static showLoading (title) {
    if (Common.loadingRequestCount === 0 && !Common._loading) {
      Common._loading = Toast.loading({
        message: title || '加载中...',
        forbidClick: true,
        duration: 0
      })
    }
    Common.loadingRequestCount++
  }

  static hideLoading (isForced = false) {
    if (isForced) {
      Common._loading && Common._loading.clear()
      Common._loading = null
      Common.loadingRequestCount = 0
      return
    }
    if (Common.loadingRequestCount <= 0 && !Common._loading) return
    if (Common.loadingRequestCount === 1) {
      Common._loading.clear()
      Common._loading = null
    }
    Common.loadingRequestCount--
  }
}

export default Common
