import axios from 'axios'
import qs from 'qs'
import Common from '@/utils/Common.js'
import setting from '@/config/setting.js'
import {
  Toast
} from 'vant'

class Request {
  _pending = {};

  constructor () {
    this._initAxios()
    this._setRequestInterceptors()
    this._setResponseInterceptors()
  }

  /**
   * @param {Object} options
   * options.codeType 1为json 2为fromData 3为www
   */
  ajax (options) {
    if (!options.url) throw new Error('请求URL不能为空')
    options.method = options.method || 'get'
    options.codeType = options.codeType || 1
    options.headers = options.headers || {}
    options.data = options.data || {}
    options.params = options.params || {}

    if (options.method === 'post' || options.method === 'put') {
      if (options.codeType === 2) {
        const formData = new FormData()
        for (let i in options.data) {
          formData.append(i, options.data[i])
        }
        options.data = formData
        options.headers['Content-Type'] = 'multipart/form-data'
      } else if (options.codeType === 3) {
        options.data = qs.stringify(options.data)
        options.headers['Content-Type'] = 'application/x-www-form-urlencoded'
      }
    }

    if (options.method === 'get' || options.method === 'delete') {
      // 容错处理
      if (Object.keys(options.params).length === 0 && Object.keys(options.data).length !== 0) {
        options.params = options.data
        options.data = {}
      }
    }

    return axios(options)
  }

  get (options) {
    options.method = 'get'
    this.ajax(options)
  }

  post (options) {
    options.method = 'post'
    this.ajax(options)
  }

  put (options) {
    options.method = 'put'
    this.ajax(options)
  }

  delete (options) {
    options.method = 'delete'
    this.ajax(options)
  }

  _initAxios () {
    axios.defaults.baseURL = 'https://easydoc.xyz'
    axios.defaults.timeout = setting.TIMEOUT
    // axios.defaults.headers.get['Content-Type'] = 'application/x-www-form-urlencoded'
  }

  _setRequestInterceptors () {
    // 请求拦截器
    axios.interceptors.request.use(config => {
      // 显示loading
      Common.showLoading()
      // 设置Token
      // config.headers[setting.TOKEN_KEY] = localStorage.getItem(setting.TOKEN_KEY) || ''
      // 取消重复请求
      const requestData = this._getRequestIdentify(config, true)
      this._removePending(requestData, true)

      config.cancelToken = new axios.CancelToken(c => {
        this._pending[requestData] = c
      })

      return config
    }, error => {
      // 对请求错误做些什么
      Common.hideLoading(true)
      Toast('请求出错')
      return Promise.reject(error)
    })
  }

  _setResponseInterceptors () {
    // 响应拦截器
    axios.interceptors.response.use(function (response) {
      // 隐藏loading
      setTimeout(() => {
        Common.hideLoading()
      }, 300)

      return response
    },
    function (error) {
      // 对响应错误做点什么
      Common.hideLoading(true)
      Toast('响应出错')
      return Promise.reject(error)
    })
  }

  _getRequestIdentify (config, isReuest = false) {
    let url = config.url
    if (isReuest) {
      url = config.baseURL + config.url.substring(1, config.url.length)
    }
    return config.method === 'get' ? encodeURIComponent(url + JSON.stringify(config.params)) : encodeURIComponent(config.url + JSON.stringify(config.data))
  }

  _removePending (key, isRequest = false) {
    if (this._pending[key] && isRequest) {
      this._pending[key]('取消重复请求')
    }
    delete this._pending[key] // 把这条记录从 pending 中移除
  }
}

export default new Request()
